# Changelog

# 0.2.0 - 2019-12-28

- **BREAKING CHANGE**: update bytes to 0.5.

# 0.1.5 - 2019-11-16

- update wasm-bindgen-test.

# 0.1.4 - 2019-11-12

- update to futures 0.3.
- remove tcp example for now because romio is no longer maintained.

# 0.1.3 - 2019-09-28

- Stabilize feature async_await.
- update to futures-codec 0.3.0.

# 0.1.2 - 2019-08-15

- The example was still wrong, third time's a charm.

# 0.1.1 - 2019-08-15

- Update the example in the readme which was forgotten.

# 0.1.0 - 2019-08-15

- Fork from tokio-serde-cbor.

